use strict;
use warnings;

#use locale;
#use encoding 'cp1251';

use utf8;

use Win32::OLE;
use Win32::OLE::Variant;

# раскомментировать, если непонятные ошибки или не работает
# но тогда не будет видно комментариев о перезапуске службы
Win32::OLE->Option(Warn => 3);
binmode STDOUT, ':encoding(cp1251)';
`chcp 1251`;

use Data::Dumper;

my $DEBUG = 0;

# настройки
my $_S = {
    host => 'sun', #адрес агента сервера 1С
    port => '2040', #порт агента!
    bases => [ #список баз
        {
            name => 'ZAKR_2013', #имя базы, которую надо проверять, как оно задано в кластере сервера 1С
            killall => 0, #удалять ли открытые сессии
            needUpdate => 0, #нужно ли обновлять БД
            login => "",
            pass => ""
        },
        {
            name => 'GTV823',
            killall => 1,
            needUpdate => 1,
            login => "ComAdmin",
            pass => "ComAdminPass1",
            updateLog => "update_GTV823.log"
        }
    ],
    serviceName => '1C:Enterprise 8.2.15.319 Server Agent (x86-64)', #имя службы сервера 1С
    serviceRestart => {
        need => 1, #нужно ли рестартовать службу сервера 1С
        ifNoConnectionsOnly => 1 #только в том случае, если нет подключений ко всем БД из списка выше
    },
    forUpdate => {
        bin => "C:\\Program Files (x86)\\1cv82\\8.2.15.319\\bin\\1cv8.exe", #где лежит запускаемый 1cv8.exe, слэши дублируем
        isVisible => 0,
        port => 2041
    }
};



sub prn {
    print "\n" . shift . "\n" if $DEBUG;
}

sub getConnectionsCount {
    my ($server, $dbname) = @_;

    my $counter = 1;
    print "GET CONNECTIONS\n" if $DEBUG;

    prn 1;
    my $ole1c = Win32::OLE->new("V82.COMConnector") or die "Could not create OLE-connector!\n";
    print "COM-connector created...\n" if $DEBUG;
    prn 2;
    my $_agent = $ole1c->ConnectAgent($server) or die "Could not connect to server!\n"; # . cnv(Dumper $ole1c->ErrorDescription());
    prn 3;
    my $_cluster = $_agent->GetClusters()->[0] or die "Could not get cluster 0 from array\n";
    print "CLUSTER: \n" . Dumper $_cluster if $DEBUG;
    prn 4;
    $_agent->Authenticate($_cluster, "", "");

    prn 5;
    my $_basesinfo = $_agent->GetInfoBases($_cluster);
    my @_bases = grep { defined $_->{Name} && $_->{Name} =~ m/^$dbname$/ } @$_basesinfo;
    my $_base = $_bases[0];
    print Dumper $_base if $DEBUG;

    prn 6;
    my $connCounter = 0;

    my $_baseconns = 0;
    eval {
        $_baseconns = $_agent->GetInfoBaseConnections($_cluster, $_base);
    } or return 0;
    foreach (@$_baseconns) {
        if ($_->{Application} !~ m/JobScheduler/ && $_->{Application} !~ m/SrvrConsole/) {
            print $_->{Application} . "\n" if $DEBUG;
            $connCounter++;
        }
    }

    return $connCounter;
}

sub killAllSessions {
    my ($server, $dbname) = @_;

    my $counter = 1;
    print "KILL SESSIONS\n" if $DEBUG;

    prn 1;
    my $ole1c = Win32::OLE->new("V82.COMConnector") or die "Could not create OLE-connector!\n";
    print "COM-connector created...\n" if $DEBUG;
    prn 2;
    my $_agent = $ole1c->ConnectAgent($server) or die "Could not connect to server!\n" . cnv(Dumper $ole1c->ErrorDescription());
    prn 3;
    my $_cluster = $_agent->GetClusters()->[0] or die "Could not get cluster 0 from array\n";
    print "CLUSTER: \n" . Dumper $_cluster if $DEBUG;
    prn 4;
    $_agent->Authenticate($_cluster, "", "");

    prn 5;
    my $_basesinfo = $_agent->GetInfoBases($_cluster);
    my @_bases = grep { defined $_->{Name} && $_->{Name} =~ m/^$dbname$/ } @$_basesinfo;
    my $_base = $_bases[0];
    #print Dumper \@_bases if $DEBUG;

    my $_sessions = $_agent->GetSessions($_cluster);

    return unless defined $_sessions;

    my @_sessions = grep { defined $_->{infoBase}->{Name} && $_->{infoBase}->{Name} eq $_base->{Name} } @$_sessions;
    #print Dumper @_sessions;

    foreach (@_sessions) {
        if ($_->{AppID} !~ m/JobScheduler/ && $_->{AppID} !~ m/SrvrConsole/) {
            $_agent->TerminateSession($_cluster, $_);
        }
    }
}





my $host = $_S->{host} . ( $_S->{port} ne '' ? ":" . $_S->{port} : '' );
print "HOST: $host\n";
my $cons = 0;

for my $base (@{$_S->{bases}}) {
    print "Checking connections in base named $base->{name}...\n";
    my $connCount = getConnectionsCount($host, $base->{name});
    print "$connCount\n";
    if ($base->{killall} && $connCount > 0) {
        print "Killing connections...\n";
        killAllSessions($host, $base->{name});
    }
    $cons += getConnectionsCount($host, $base->{name});
}

print "Total active connections: $cons\n";

if ($_S->{serviceRestart}->{need}) {
    if ($_S->{serviceRestart}->{ifNoConnectionsOnly} && $cons > 0) {
        print "Will not restart service.\n";
        exit;
    } else {
        print "Restarting...\n";
        print "Restarting service named $_S->{serviceName}...\n";
        print `net stop "$_S->{serviceName}"`;
        print `taskkill.exe /F /IM rmngr.exe`;
        print `net start "$_S->{serviceName}"`;
    }
}

print "UPDATE!!!\n";
$host = $_S->{host} . ( $_S->{forUpdate}->{port} ne '' ? ":" . $_S->{forUpdate}->{port} : '' );
print "HOST: $host\n";
for my $base (@{$_S->{bases}}) {
    print "Checking for update database named $base->{name}...\n";
    if ($base->{needUpdate}) {
        print "Updating...\n";
        my $command = "\"$_S->{forUpdate}->{bin}\" CONFIG /S$host\\$base->{name} /N\"$base->{login}\" /P$base->{pass} /UpdateDBCfg -WarningsAsErrors";
        $command .= " /DumpResult $base->{updateLog}" if ($base->{updateLog} ne '');
        $command .= " /Visible" if ($_S->{forUpdate}->{isVisible});
        print "COMMAND: $command\n";
        print `$command`;
    }
}
